//
//  metalViewsApp.swift
//  metalViews
//
//  Created by Raul on 1/8/24.
//

import SwiftUI

@main
struct metalViewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
