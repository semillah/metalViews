//
//  Shaders.metal
//  metalViews
//
//  Created by Raul on 1/8/24.
//

// Shaders.metal

#include <metal_stdlib> // Including the Metal standard library.
// The standard library provides essential data types and functions for shader development.

using namespace metal; // Using the Metal namespace.
// This simplifies the code by allowing direct use of Metal's types and functions.

// Defining a vertex shader function.
// Vertex shaders process individual vertices and output a single vertex.
vertex float4 vertexShader() {
    // Returning a hardcoded float4 value.
    // float4 is a vector type that contains four floating-point numbers.
    // Here, you are returning a vector with all components set to 1.0.
    // In a typical vertex shader, you would calculate and return the position of a vertex.
    return float4(1.0);
}

// Defining a fragment shader function.
// Fragment shaders process fragments (potential pixels) and output a color for each.
fragment float4 fragmentShader() {
    // Returning a default-initialized float4.
    // This effectively returns a vector with all components set to 0.
    // In a typical fragment shader, you would calculate and return the color of a fragment.
    // Here, since no color is specified, the output would be black (assuming alpha is implicitly 1).
    return float4();
}

