//
//  Renderer.swift
//  metalViews
//
//  Created by Raul on 1/8/24.
//

import Foundation
import MetalKit

// Renderer class conforms to NSObject and MTKViewDelegate protocols.
// As a coordinator for MetalView, Renderer handles Metal-specific rendering and updating tasks.
class Renderer: NSObject, MTKViewDelegate {
    
    // Holds a reference to the MetalView that created this Renderer.
    // This allows the Renderer to access properties and methods of MetalView.
    var parent: MetalView
    
    
    
    var metalDevice: MTLDevice! // Holds the reference to the Metal device (GPU).
    // This is essential for creating and managing Metal objects.
    
    var metalCommandQueue: MTLCommandQueue! // A queue to manage the submission of command buffers to the device.
    // Command queues serialize the commands that need to be executed by the GPU.
    
    var pipelineState: MTLRenderPipelineState // The compiled render pipeline state.
    // This object encapsulates the GPU's rendering configuration.
    
    
    
    // The Renderer's initializer.
    init(_ parent: MetalView) {
        self.parent = parent // Storing a reference to the MetalView that created this Renderer.
        
        // Creating the Metal device, which is an abstraction of the GPU.
        if let metalDevice = MTLCreateSystemDefaultDevice() {
            self.metalDevice = metalDevice // The metalDevice represents the GPU and is used for all Metal operations.
        }
        
        // Creating a command queue.
        // A command queue is used to organize the sequence of commands sent to the GPU.
        // Each command queue can accept multiple command buffers.
        self.metalCommandQueue = self.metalDevice.makeCommandQueue()
        
        // Setting up the pipeline descriptor.
        // This is a blueprint for creating a compiled pipeline state.
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        
        // Accessing the default library which contains the shaders.
        // Shaders are programs that run on the GPU. They are written in Metal Shading Language.
        let library = self.metalDevice.makeDefaultLibrary()
        
        // Linking the shaders with the pipeline.
        // The vertex shader processes each vertex and the fragment shader processes each pixel.
        pipelineDescriptor.vertexFunction = library?.makeFunction(name: "vertexShader")
        pipelineDescriptor.fragmentFunction = library?.makeFunction(name: "fragmentShader")
        
        // Setting the pixel format for the color attachment.
        // This needs to match the pixel format of the MTKView to ensure correct rendering.
        pipelineDescriptor.colorAttachments[0].pixelFormat = .rgba8Unorm
        
        // Compiling the pipeline state.
        // This process translates the pipeline configuration into a format the GPU can execute.
        do {
            try self.pipelineState = self.metalDevice.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            fatalError("Cannot create Render Pipeline State")
        }
        
        super.init() // Completing the initialization of the Renderer object.
    }
    
    // Delegate method called when the drawable size of the MTKView will change.
    // This is where you handle resizing of render targets when the view size changes.
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        // Implementation for handling size changes in MTKView.
    }
    
    // The primary rendering method.
    // This method is called every time the view needs to render a new frame.
    // Here, you write code to perform Metal drawing and rendering operations.
    func draw(in view: MTKView) {
        // Ensuring a drawable is available.
        // A drawable is an object that Metal can render into.
        guard let drawable = view.currentDrawable else {
            return
        }
        
        // Creating a command buffer.
        // A command buffer is a collection of commands that the GPU will execute.
        let commandBuffer = metalCommandQueue.makeCommandBuffer()
        
        // Setting up the render pass descriptor.
        // This describes how rendering is to be done, including which textures to render to.
        let renderPassDescriptor = view.currentRenderPassDescriptor
        renderPassDescriptor?.colorAttachments[0].clearColor = MTLClearColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        renderPassDescriptor?.colorAttachments[0].loadAction = .clear // Clears the previous frame's content.
        renderPassDescriptor?.colorAttachments[0].storeAction = .store // Indicates the rendered content will be stored.
        
        // Creating a render command encoder.
        // This encodes the commands that describe how to render the current frame.
        let renderEncoder = commandBuffer?.makeRenderCommandEncoder(descriptor: renderPassDescriptor!)
        
        // Setting the viewport.
        // The viewport defines the region of the drawable into which Metal will render content.
        renderEncoder?.setViewport(.init(originX: 0, originY: 0, width: view.drawableSize.width, height: view.drawableSize.height, znear: 0, zfar: 1))
        
        // Setting the pipeline state.
        // This tells the GPU how to process the vertices and fragments when rendering.
        renderEncoder?.setRenderPipelineState(pipelineState)
        
        // Ending the encoding of commands.
        // This signifies that all the commands for this frame have been issued.
        renderEncoder?.endEncoding()
        
        // Presenting the drawable on the screen.
        // This command will display the rendered content in the MTKView.
        commandBuffer?.present(drawable)
        
        // Committing the command buffer.
        // This sends the encoded commands to the GPU for execution.
        commandBuffer?.commit()
    }
    
}
